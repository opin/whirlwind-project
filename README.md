A Composer-based installer for the Whirlwind distribution.

## Usage

Use the following command to create a new Whirlwind project:

`$ composer create-project opin/whirlwind-project [MY-PROJECT]`

Composer will pull in the whirlwind profile and all required dependencies. A `web` directory will be created and the Drupal code will be placed there. You can then install it like you would any other Drupal site.

## Documentation

Coming soon!

