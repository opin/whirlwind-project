<?php

/**
 * @file
 * The Whirlwind profile.
 */

use Drupal\user\RoleInterface;

/**
 * Implements hook_install_tasks().
 */
function whirlwind_install_tasks()
{
  $tasks = [];

  $tasks['whirlwind_set_front_page'] = [];
  $tasks['whirlwind_disallow_free_registration'] = [];
  $tasks['whirlwind_grant_shortcut_access'] = [];
  $tasks['whirlwind_set_default_theme'] = [];

  return $tasks;
}

/**
 * Sets the front page path to /node.
 */
function whirlwind_set_front_page()
{
  if (Drupal::moduleHandler()->moduleExists('node')) {
    Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node')
      ->save(TRUE);
  }
}

/**
 * Only allows administrators to create new user accounts.
 */
function whirlwind_disallow_free_registration()
{
  Drupal::configFactory()
    ->getEditable('user.settings')
    ->set('register', \Drupal\user\UserInterface::REGISTER_ADMINISTRATORS_ONLY)
    ->save(TRUE);
}

/**
 * Allows authenticated users to use shortcuts.
 */
function whirlwind_grant_shortcut_access()
{
  if (Drupal::moduleHandler()->moduleExists('shortcut')) {
    user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);
  }
}

/**
 * Sets the default and administration themes.
 */
function whirlwind_set_default_theme()
{
  Drupal::configFactory()
    ->getEditable('system.theme')
    ->set('default', 'bartik')
    ->set('admin', 'adminimal_theme')
    ->save(TRUE);

  // Use the admin theme for creating content.
  if (Drupal::moduleHandler()->moduleExists('node')) {
    Drupal::configFactory()
      ->getEditable('node.settings')
      ->set('use_admin_theme', TRUE)
      ->save(TRUE);
  }
}
