A fast and feature-rich Drupal distribution created by OPIN Software.

## Get Started

In order to create a new project using this distribution run the following
command:

`$ composer create-project opin/whirlwind-project [MY-PROJECT]`
